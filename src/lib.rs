use crossbeam_channel::{bounded, unbounded, Sender};
use std::collections::HashMap;
use std::hash::Hash;
use std::sync::Arc;
use std::thread;

pub struct CrossMap<K, V> {
    sender: Sender<Action<K, V>>,
}

enum Action<K, V> {
    Get(K, crossbeam_channel::Sender<Option<Arc<V>>>),
    Insert(K, Arc<V>, crossbeam_channel::Sender<()>),
    Remove(K, crossbeam_channel::Sender<()>),
}

impl<K, V> CrossMap<K, V>
where
    K: Send + Sync + 'static + Eq + Hash,
    V: Send + Sync + 'static,
{
    pub fn new() -> CrossMap<K, V> {
        let (sender, receiver) = unbounded::<Action<K, V>>();

        thread::spawn(move || {
            let mut data: HashMap<K, Arc<V>> = HashMap::new();

            while let Ok(message) = receiver.recv() {
                match message {
                    Action::Get(key, send) => {
                        let result = data.get(&key).cloned();
                        send.send(result).unwrap();
                    }
                    Action::Insert(key, value, send) => {
                        data.insert(key, value);
                        send.send(()).unwrap();
                    }
                    Action::Remove(key, send) => {
                        data.remove(&key);
                        send.send(()).unwrap();
                    }
                }
            }
        });

        CrossMap { sender }
    }

    pub fn get(&self, key: K) -> Option<Arc<V>> {
        let (send, recv) = bounded::<Option<Arc<V>>>(1);
        self.sender.send(Action::Get(key, send)).unwrap();
        recv.recv().unwrap()
    }

    pub fn insert(&self, key: K, value: Arc<V>) {
        let (send, recv) = bounded::<()>(1);
        self.sender.send(Action::Insert(key, value, send)).unwrap();
        recv.recv().unwrap();
    }

    pub fn remove(&self, key: K) {
        let (send, recv) = bounded::<()>(1);
        self.sender.send(Action::Remove(key, send)).unwrap();
        recv.recv().unwrap();
    }
}
